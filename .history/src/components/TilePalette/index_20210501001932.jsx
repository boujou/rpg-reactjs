import React from 'react';

export const TilePalette = ({ position, size }) => {
const { width, heigth } = size
    
    return (
        <div
        id="palette"
        style={{
          position: "absolute",
          border: "1px solid black",
          top: position.y,
          left: position.x,
          zIndex: 100,
          backgroundColor: "white",
        }}
      >
        <img id="handle" src="/img/drag-handle.png" alt="drag-icon" />
      </div>
    );
};

