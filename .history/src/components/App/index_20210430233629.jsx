import { useEffect, useState } from "react";

function App() {
  const [position, setPosition] = useState({
    x: 0,
    y: 0,
  });

  useEffect(() => {
    const handle = document.getElementById("handle")
    handle.addEventListener("mousedown", (e) => {
      e.preventDefault
      handle.style.pointerEvents = "none"
    })
  })

  return (
    <div
      style={{
        position: "relatives",
        width: window.innerWidth,
        height: window.innerHeight,
        backgroundColor: "grey",
        overflow: "hidden",
        border: "1px solid black",
      }}
    >
      <div
        style={{
          position: "absolute",
          border: "1px solid black",
          top: position.y,
          left: position.x,
          zIndex: 100,
          width: 200,
          height: 200,
          backgroundColor: "white",
        }}
      >
        <img src="/img/drag-handle.png" alt="drag-icon" />
      </div>
    </div>
  );
}

export default App;
