import { useDraggable } from "../../hooks/use-draggable";

function App() {
  const { position } = useDraggable("handle");

  return (
    <div
      style={{
        position: "relatives",
        width: window.innerWidth,
        height: window.innerHeight,
        backgroundColor: "grey",
        overflow: "hidden",
        border: "1px solid black",
      }}
    >
    </div>
  );
}

export default App;
