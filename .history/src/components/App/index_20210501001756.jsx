import { useDraggable } from "../../hooks/use-draggable";
import { TilePalette } from "../TilePalette";

function App() {
  const { position } = useDraggable("handle");

  return (
    <div
      style={{
        position: "relatives",
        width: window.innerWidth,
        height: window.innerHeight,
        backgroundColor: "grey",
        overflow: "hidden",
        border: "1px solid black",
      }}
    >
      <TilePalette 
      
      />
    </div>
  );
}

export default App;
