import { useState } from "react";
import { useDraggable } from "../../hooks/use-draggable";
import { TilePalette } from "../TilePalette";

function App() {
  const [tileset, setTileset] = useState("rpg-nature-tileset/spring")
  const [tiles, setTiles] = useState([])
  const [mapSiz]
  const { position } = useDraggable("handle");

  return (
    <div
      style={{
        position: "relatives",
        width: window.innerWidth,
        height: window.innerHeight,
        backgroundColor: "grey",
        overflow: "hidden",
        border: "1px solid black",
      }}
    >
      <TilePalette
        position={position}
        size={{
          height: 288,
          width: 640,
        }}
      />
    </div>
  );
}

export default App;
