import React from "react";

export default function Map({ tiles, tileset, size, setTiles, activeTile, bgTile }) {
  const cloneMatrix = (m) => {
    const clone = new Array(m.length);
    for (let i = 0; i < m.length; i++) {
      clone[i] = m[i].slice(0);
    }
    return clone;
  };
  const dropTile = ({ x, y }) => {
    setTiles((prev) => {
      const clone = cloneMatrix(prev);
      const tile = {
        ...clone[y][x],
        v: activeTile,
      };
      clone[y][x] = tile;
      return clone;
    });
  };
  return (
    <div
      style={{
        boxSizing: "border-box",
        backgroundColor: "white",
        width: size.width,
      }}
    >
    </div>
  );
}
