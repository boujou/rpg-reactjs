import React from "react";

export default function Map({ tiles, tileset, size }) {
  const dropTile = ({x, y}) => {
    setTil
  }
  return (
    <div
      style={{
        boxSizing: "border-box",
        backgroundColor: "white",
        width: size.width,
      }}
    >
      {tiles.map((row, y) => (
        <div key={y} style={{ display: "flex" }}>
          {row.map((tile, x) => (
            <div
              onClick={() => dropTile({x, y})}
              key={x}
              style={{
                borderTop: "1px solid black",
                borderRight: "1px solid black",
                background: `url(/sprites/${tileset}.png) -${
                  tile.v.x * 32
                }px -${tile.v.y * 32}px no-repeat`,
                width: 32,
                height: 32,
              }}
            ></div>
          ))}
        </div>
      ))}
    </div>
  );
}
