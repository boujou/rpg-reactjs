import { useEffect, useState } from "react";

const [position, setPosition] = useState({
    x: 0,
    y: 0,
  });

  useEffec(() => {
    const handle = document.getElementById("handle");

    handle.addEventListener("mousedown", function (e) {
      e.preventDefault();
      handle.style.pointerEvents = "none";

      document.body.addEventListener("mousemove", move);
      document.body.addEventListener("mouseup", () => {
        document.body.removeEventListener("mousemove", move);
        handle.style.pointerEvents = "initial";
      });
    });

    return () => {
      document.body.removeEventListener("mousedown", move);
      document.body.removeEventListener("mouseup", move);
      document.body.removeEventListener("mousemove", move);
    };
  }, []);

  function move(e) {
    const pos = {
      x: e.clientX,
      y: e.clientY,
    };
    setPosition(pos);
  }
